---
title: "project"
authors: "Isaac Sanders, Jiaxuan Wang, Fei Zhou, Denis Tukupov"
date: "11 01 2022"
output: pdf_document
clan: "Intruders"
---

```{r Import libraries and data}
library(ggplot2)
library(readxl)
library(psych)
library(tidyr)
library(tidyverse)
library(broom)

library(magrittr) # needs to be run every time you start R and want to use %>%
library(dplyr)    # alternatively, this also loads %>%
```

```{r}
data <- read_excel("D:/Cloud Mail.Ru/study/ВШЭ/1 semester/Linear models/Final project/2-nd point/Dataset.xls", sheet = "s", col_names = TRUE)

#data_ful <- read_excel("D:/Cloud Mail.Ru/study/ВШЭ/1 semester/Linear models/Final #project/2-nd point/Transportation Data.xls")

str(data)
```
```{r dispersion analysis}
# Build 
ggplot(data, mapping = aes(x = factor(Age), y = Pub_Trans)) +
  geom_boxplot()

ggplot(data, mapping = aes(x = factor(Sex), y = Pub_Trans)) +
  geom_boxplot() 

ggplot(data, mapping = aes(x = factor(Seatbelt), y = Pub_Trans)) +
  geom_boxplot() +
  ggtitle("Seatbelt")

ggplot(data, mapping = aes(x = factor(Education), y = Pub_Trans)) +
  geom_boxplot() +
  ggtitle("Education")

ggplot(data, mapping = aes(x = factor(Job), y = Pub_Trans)) +
  geom_boxplot() +
  ggtitle("Job")

ggplot(data, mapping = aes(x = factor(Fam_Income), y = Pub_Trans)) +
  geom_boxplot() +
  ggtitle("Fam_Income")

# Build 
ggplot(data, aes(x=Age, y=Pub_Trans, color=factor(Sex), group=Sex)) +
  stat_summary(fun.data=mean_cl_boot,
               geom='errorbar', width=0.1,
               position=position_dodge(width=0.2)) +
  stat_summary(fun.data=mean_cl_boot,
               geom='line', size=1,
               position=position_dodge(width=0.2)) +
  stat_summary(fun.data=mean_cl_boot,
               geom='point', shape='square',
               size=3, position=position_dodge(width=0.2)) +
  theme_bw()


ggplot(data, aes(x=factor(Education), y=Pub_Trans, color=factor(Seatbelt), group=Seatbelt)) +
  stat_summary(fun.data=mean_cl_boot,
               geom='errorbar', width=0.1,
               position=position_dodge(width=0.2)) +
  stat_summary(fun.data=mean_cl_boot,
               geom='line', size=1,
               position=position_dodge(width=0.2)) +
  stat_summary(fun.data=mean_cl_boot,
               geom='point', shape='square',
               size=3, position=position_dodge(width=0.2)) +
  theme_bw()


ggplot(data, aes(x=factor(Education), y=Pub_Trans, color=factor(Seatbelt), group=Seatbelt)) +
  stat_summary(fun.data=mean_cl_boot,
               geom='errorbar', width=0.1,
               position=position_dodge(width=0.2)) +
  stat_summary(fun.data=mean_cl_boot,
               geom='line', size=1,
               position=position_dodge(width=0.2)) +
  stat_summary(fun.data=mean_cl_boot,
               geom='point', shape='square',
               size=3, position=position_dodge(width=0.2)) +
  theme_bw()


fit <- aov(Pub_Trans~factor(Seatbelt), data = data)
summary(fit)
TukeyHSD(fit)

fit <- aov(Pub_Trans~factor(Age), data = data)
summary(fit)
significant_value_inside_the_group <- TukeyHSD(fit)
as.data.frame(tidy(res, significant_value_inside_the_group)) %>% 
  filter(adj.p.value < 0.05)
```


```{r}
# build plot for each variables to make first view
pairs(data)
# Count corr test and p-value for each variable
corr.test(data)
```
```{r build barplot (histogram)}
ggplot(data, mapping=aes(x=Pub_Trans, fill=factor(Sex))) +
  geom_histogram(alpha = 0.5, position = "dodge", binwidth = 0.5)

ggplot(data, mapping=aes(x=Pub_Trans, fill = factor(Age))) +
  geom_histogram(alpha = 0.8, position = "dodge", binwidth = 0.5)

ggplot(data, mapping=aes(x=Pub_Trans, fill=factor(Seatbelt))) +
  geom_histogram(alpha = 0.9, position = "dodge", binwidth = 0.5)

ggplot(data, mapping=aes(x=Pub_Trans, fill = factor(Education))) +
  geom_histogram(alpha = 0.9, position = "dodge", binwidth = 0.5)

ggplot(data, mapping=aes(x = Pub_Trans, fill=factor(Job))) +
  geom_histogram(alha = 0.9, position = "dodge", binwidth = 0.5)

ggplot(data, mapping=aes(x = Pub_Trans, fill=factor(Fam_Income))) +
  geom_histogram(alha = 0.9, position = "dodge", binwidth = 0.5)

```

```{r Do not run. This is a draft version}
data_ful <- data_ful[,c("R_AGE", "R_SEX", "FQSTBELT", "EDUC", "JOBLSTWK", 
                        "HHFAMINC", "PTUSED")]
data_full_no_missing <- data_ful %>% 
  drop_na()
```

```{r}
# Linear_models
model_1 <- lm(data$Pub_Trans~data$Age)
summary(model_1)

model_2 <- lm(data = data, Pub_Trans~Age+Sex+Seatbelt+Education+Job+Fam_Income)
summary(model_2)

# Scatter plot with linear regression
ggplot(data = data, mapping=aes(x = Age, y = Pub_Trans)) +
  geom_point(size = 2) +
  geom_smooth(method="lm")

ggplot(data, mapping=aes(x=Age, y = Pub_Trans)) +
  geom_point(alpha = 0.9, position=position_jitter(width=0, height=0.3)) +
  geom_smooth(method = "lm") +
  facet_grid(.~factor(Sex))

# The squares are the average for each old. A line (LM) will be drawn along them.
gg <- ggplot(data, aes(Age, Pub_Trans, fill=factor(Sex))) + theme_bw() +
  geom_point(alpha = 0.5, position=position_jitter(width=0, height=0.3)) +
  geom_point(shape=21, position= position_dodge(width=0.2)) +  
  stat_summary(fun.y = "mean", geom="point", size=5, shape=22, colour="black" ) +
  scale_fill_manual(values = c("1" = "blue", "2" = "red"))
```


```{r}
mtcars <- mtcars
fit <- lm(mpg~hp, mtcars)
fitted_values <- data.frame(mpg = mtcars$mpg, fitted = fit$fitted.values)
```

