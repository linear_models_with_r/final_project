---
title: "CDA_HW_2_Seminar_7"
authors: Isaac Sanders, Jiaxuan Wang, Fei Zhou, Denis Tukupov
date: "10/01/2021"
output: pdf_document
clan: Intruders
---

```{r Import libraries and download file in R space.}
library(foreign)
library(tidyverse)
library(broom)
library(rdrobust)
library(rddensity)
library(modelsummary)
library(huxtable)
library(rddtools)
library(rdd)
library(rgl)
library(ggplot2)

data <- read.dta("clark.dta")

```


```{r Task № 1. Create scatter-plot of the change in the pass rate on the vote in  favor of GM status, message=FALSE}
sum(is.na(data)) # Checking if there are any gaps (missing) in the dataframe
str(data) # Looking type for each column

data$win <- factor(data$win, levels = c(0, 1), labels = c(0, 1))

data_0 <- data[data$win == 1,]
ggplot(data_0, aes(dpass, vote)) +
  geom_point(size=3) +
  geom_smooth(method='lm')


data_1 <- aggregate(vote~dpass, data[data$win == 1,], mean)
str(data_1)

ggplot(data_1, aes(x=dpass, y = vote)) + geom_point(size=2)

ggplot(data_1, aes(dpass, vote)) +
  geom_point(size=3) +
  geom_smooth(se = F, ) +
  geom_smooth(method='lm', col='green')
```


```{r Task № 2.a. }
# We check the separator into two groups
ggplot(data, mapping=aes(x=vote, y = win, col = win)) +
  geom_point(alpha = 0.1, position=position_jitter(width=0, height=0.3)) +
  geom_vline(xintercept=50)

# Count number of observations in each group (winers/losers)
data %>% 
  group_by(win, vote > 50) %>% 
  summarize(count = n())

# Check for discontinuity in running variables around cutpoint
ggplot(data, mapping=aes(x = vote, fill = win)) +
  geom_histogram(binwidth = 4, color = "white") +
  geom_vline(xintercept= 50)

# Do the Mcrary density test
rdplotdensity(rdd = rddensity(X = data$vote, c = 50),
              X = data$vote)

## Check for discontinuity in outcome across running variable
# This we can see gap. And it's seems statistically significant
data_3 <- aggregate(dpass~round(vote, 0)+win, data, mean)
colnames(data_3) <- c("vote", "win", "dpass")
data_3$win <- factor(data_3$win, labels=c("loser", "winer"))
# show smooth with vote [15;85] and with 2 groups (losers and winers)
ggplot(data_3[data_3$vote >= 15 & data_3$vote <= 85,], mapping=aes(x = vote, y = dpass, color = win)) +
  geom_point(size = 2, alpha = 0.5) +
  geom_vline(xintercept = 50, linetype = "dashed") +
  geom_smooth() + 
  geom_smooth(method = "lm", se = F)


# Try to build Figure 8
group_vote <- c()
index <- 1 
for (i in data_3$vote) {
  if (i >= 0 & i <= 5) {
    group_vote[index] <- 1
  }
  else if(i > 5 & i <= 10){
    group_vote[index] <- 2
  }
  else if(i > 10 & i <= 15){
    group_vote[index] <- 3
  }
  else if(i > 15 & i <= 20){
    group_vote[index] <- 4
  }
  else if(i > 20 & i <= 25){
    group_vote[index] <- 5
  }
  else if(i > 25 & i <= 30){
    group_vote[index] <- 6
  }
  else if(i > 30 & i <= 35){
    group_vote[index] <- 7
  }
  else if(i > 35 & i <= 40){
    group_vote[index] <- 8
  }
  else if(i > 40 & i <= 45){
    group_vote[index] <- 9
  }
  else if(i > 45 & i <= 50){
    group_vote[index] <- 10
  }
  else if(i > 50 & i <= 55){
    group_vote[index] <- 11
  }
  else if(i > 55 & i <= 60){
    group_vote[index] <- 12
  }
  else if(i > 60 & i <= 65){
    group_vote[index] <- 13
  }
  else if(i > 65 & i <= 70){
    group_vote[index] <- 14
  }
  else if(i > 70 & i <= 75){
    group_vote[index] <- 15
  }
  else if(i > 75 & i <= 80){
    group_vote[index] <- 16
  }
  else if(i > 80 & i <= 85){
    group_vote[index] <- 17
  }
  else if(i > 85 & i <= 90){
    group_vote[index] <- 18
  }
  else if(i > 90 & i <= 95){
    group_vote[index] <- 19
  }
  else if(i > 95 & i <= 100){
    group_vote[index] <- 20
  }
  else {
    group_vote[index] <- "Smth wrong"
  }
  index <- index + 1
}
data_3$group <- group_vote
data_3$group[42] <- 11
str(data_3)

data_4 <- aggregate(cbind(win, dpass)~group, data_3, mean)
data_4$win <- factor(data_4$win, labels = c("losers", "winers"))

ggplot(data_4, mapping=aes(x = group, y = dpass, color = win)) +
  geom_point(size = 2, alpha = 0.5) +
  geom_vline(xintercept = 10, linetype = "dashed") +
  geom_vline(xintercept = 3, linetype = "dotted" ) +
  geom_vline(xintercept = 18, linetype = "dotted") +
  geom_line()

ggplot(data_4[data_4$group >= 3 & data_4$group <= 18,], 
       mapping=aes(x = group, y = dpass, color = win)) +
  geom_point(size = 2, alpha = 0.5) +
  geom_vline(xintercept = 10, linetype = "dashed") +
  geom_vline(xintercept = 3, linetype = "dotted" ) +
  geom_vline(xintercept = 18, linetype = "dotted") +
  geom_line() +
  geom_smooth(method = "lm", se =F)


## Try to build Table 3a
# Filter data vote [15%;85%] 
data_15_85_vote <- data[data$vote >= 15 & data$vote <= 85,]
model_1 <- lm(data_15_85_vote$dpass ~ data_15_85_vote$win)

model_2 <- lm(data_15_85_vote$dpass ~ data_15_85_vote$win + data_15_85_vote$vote)

model_3 <- lm(data_15_85_vote$dpass ~ data_15_85_vote$win + data_15_85_vote$lose_vote +
                data_15_85_vote$win_vote)

model_4 <- lm(data_15_85_vote$dpass ~ data_15_85_vote$win + data_15_85_vote$lose_vote +
                data_15_85_vote$win_vote + data_15_85_vote$lose_vote_2 +
                data_15_85_vote$win_vote_2)

# Combining model indicators into one table
huxreg(list("win" = model_1,
            "win+vote" = model_2,
            "win+lose/win_vote" = model_3,
            "win+lose/win_vote+lose/win_vote_2" = model_4))
```


```{r Task № 2.b. }
# center-simple-models
data_3_centered <- data_3 %>% 
  mutate(vote_centered = vote - 50)

model_1_centered_all <- lm(dpass~ vote_centered + win, data = data_3_centered)
tidy(model_1_centered_all)
# Intercept says, that linear model of losers has 0.210 dpass when touch separation linea
# Estimate of vote_centered shows, that every point up above the threshold our
# dpass goes down by -0.07 point.
# And finally estimate of "winnner" shows gap between losers and winers on scale of "dpass"


data_3_centered_20 <- data_3_centered %>% 
  filter(vote_centered > -20 & vote_centered < 20)

data_3_centered_10 <- data_3_centered %>% 
  filter(vote_centered > -10 & vote_centered < 10)

model_2_centered_20 <- lm(dpass ~ vote_centered + win, data = data_3_centered_20)
tidy(model_2_centered_20)

model_3_centered_10 <- lm(dpass ~ vote_centered + win, data = data_3_centered_10)
tidy(model_3_centered_10)

# Unit all 3 model to 1 table for looking result in one table
huxreg(list("Full data" = model_1,
            "data_centered_20" = model_2_centered_20,
            "data_centered_10" = model_3_centered_10))

rdplot(y = data_3$dpass,
       x = data_3$vote,
       c = 49)

ggplot(model_2_centered_20, mapping=aes(x = vote_centered, y = dpass, color = win)) +
  geom_point(size = 2, alpha = 0.5) +
  geom_vline(xintercept = 0, linetype = "dashed") +
  geom_smooth() + 
  geom_smooth(method = "lm", se = F) +
  ggtitle("Wrong Lm with vote from 20 to 80")

ggplot(model_3_centered_10, mapping=aes(x = vote_centered, y = dpass, color = win)) +
  geom_point(size = 2, alpha = 0.5) +
  geom_vline(xintercept = 0, linetype = "dashed") +
  geom_smooth() +
  geom_smooth(method = "lm", se = F) +
  ggtitle("Wrong LM with vote from 10 to 90")
```


```{r Task № 3}
# build simple scatter plot x = vote and y = passrate2
ggplot(data, mapping = aes(x = vote, y = passrate2 )) +
  geom_point(size = 3, color = "green", alpha = 0.5)


# build lm with independent: vote and win; dependent: passrate2
data$win <- factor(data$win, labels = c("losers", "winers"))

passrate2_linear_model_1 <- lm(data$passrate2 ~ data$vote + data$win)

summary(passrate2_linear_model_1)

ggplot(data, mapping=aes(x = vote, y = passrate2, color = win)) +
  geom_point(size = 3, color = "green") +
  geom_smooth(method = "lm") +
  ggtitle("passrate2_linear_mode_1")


# 
passrate2_linear_model_2 <- lm(data$passrate2 ~ data$win + data$lose_vote +
                                 data$win_vote)
summary(passrate2_linear_model_2)

scatter3d(x=data$lose_vote, y = data$passrate2, z = data$win_vote, groups = data$win,
          grid = FALSE, title="passrate2_linear_model_2")

```



```{r Task № 4}
# Use data with 15-85 vote. But it doesn't matter
passrate_0_lm_1 <- lm(data_15_85_vote$passrate0 ~ data_15_85_vote$win)
summary(passrate_0_lm_1)

# such an analysis can takes place, but nothing good here. We think, only win as independent
# variable can not be use in lineare regression. Just if we look on grath, all points lined 
# up in two groups.

ggplot(data_15_85_vote, mapping=aes(x = win, y = passrate0)) +
  geom_point(size = 3, color = "green") +
  geom_smooth(method = "lm") +
  ggtitle("passrate0_linear_mode_1")
```




```{r Task № 5}
# We start by getting the coefficients for the data with cutoff of 50
data <- read.dta("clark.dta")
data$win <- factor(data$win, levels = c(0, 1), labels = c(0, 1))
data_0 <- data[data$win == 1,]

data_1 <- aggregate(vote~dpass, data[data$win == 1,], mean)
str(data_1)

data %>% 
  group_by(win, vote > 50) %>% 
  summarize(count = n())

data_3 <- aggregate(dpass~vote+win, data, mean)
colnames(data_3) <- c("vote", "win", "dpass")
data_3$win <- factor(data_3$win, labels=c("loser", "winer"))


data_3_centered <- data_3 %>% 
  mutate(vote_centered = vote - 50)

model_1_centered_all <- lm(dpass~ vote_centered + win, data = data_3_centered)
tidy(model_1_centered_all)

data_3_centered_20 <- data_3_centered %>% 
  filter(vote_centered > -20 & vote_centered < 20)

data_3_centered_10 <- data_3_centered %>% 
  filter(vote_centered > -10 & vote_centered < 10)

model_2_centered_20 <- lm(dpass ~ vote_centered + win, data = data_3_centered_20)
tidy(model_2_centered_20)

model_3_centered_10 <- lm(dpass ~ vote_centered + win, data = data_3_centered_10)
tidy(model_3_centered_10)

# Unit all 3 model to 1 table for looking result in one table
table_50 <- huxreg(list("Full data" = model_1_centered_all,
                        "data_centered_20" = model_2_centered_20,
                        "data_centered_10" = model_3_centered_10))



plot_50 <- ggplot(data_3[data_3$vote >= 15 & data_3$vote <= 85,], mapping=aes(x = vote, y = dpass, color = win)) +
  geom_point(size = 2, alpha = 0.5) +
  geom_vline(xintercept = 50, linetype = "dashed") +
  geom_smooth() + 
  geom_smooth(method = "lm", se = F)


rdd_object <-rdd_data(y=data_3$dpass,x=data_3$vote,cutpoint=50)
new_table_50 <- rdd_reg_np(rdd_object)



### Now we do the same for a cutoff of 45

data <- read.dta("clark.dta")
data$win <- factor(data$win, levels = c(0, 1), labels = c(0, 1))
data_0 <- data[data$win == 1,]

data_1 <- aggregate(vote~dpass, data[data$win == 1,], mean)
str(data_1)

data %>% 
  group_by(win, vote > 45) %>% 
  summarize(count = n())

# first I change data to have win =1 if vote>45 (sorry for ugly code!)
data$win <- factor(data$win, levels = c(0, 1), labels = c(0, 1))
data$win[data$vote >45] <- (data$win[data$vote >45] == 1)
data[is.na(data)] = 1

data_3 <- aggregate(dpass~vote+win, data, mean)
colnames(data_3) <- c("vote", "win", "dpass")
data_3$win <- factor(data_3$win, labels=c("loser", "winer"))


data_3_centered <- data_3 %>% 
  mutate(vote_centered = vote - 45)

model_1_centered_all <- lm(dpass~ vote_centered + win, data = data_3_centered)
tidy(model_1_centered_all)

data_3_centered_20 <- data_3_centered %>% 
  filter(vote_centered > -20 & vote_centered < 20)

data_3_centered_10 <- data_3_centered %>% 
  filter(vote_centered > -10 & vote_centered < 10)

model_2_centered_20 <- lm(dpass ~ vote_centered + win, data = data_3_centered_20)
tidy(model_2_centered_20)

model_3_centered_10 <- lm(dpass ~ vote_centered + win, data = data_3_centered_10)
tidy(model_3_centered_10)

# Unit all 3 model to 1 table for looking result in one table
table_45 <- huxreg(list("Full data" = model_1_centered_all,
                        "data_centered_20" = model_2_centered_20,
                        "data_centered_10" = model_3_centered_10))



plot_45 <- ggplot(data_3[data_3$vote >= 15 & data_3$vote <= 85,], mapping=aes(x = vote, y = dpass, color = win)) +
  geom_point(size = 2, alpha = 0.5) +
  geom_vline(xintercept = 45, linetype = "dashed") +
  geom_smooth() + 
  geom_smooth(method = "lm", se = F)

rdd_object <-rdd_data(y=data_3$dpass,x=data_3$vote,cutpoint=45)
new_table_45 <- rdd_reg_np(rdd_object)

### We repeat the analysis for cutoff of 55

data$win <- factor(data$win, levels = c(0, 1), labels = c(0, 1))
data$win[data$vote <55] <- (data$win[data$vote <55] == 0)
data[is.na(data)] = 0

data <- read.dta("data/clark.dta")
data$win <- factor(data$win, levels = c(0, 1), labels = c(0, 1))
data$win[data$vote <55] <- (data$win[data$vote <55] == 0)
data[is.na(data)] = 0


data_1 <- aggregate(vote~dpass, data[data$win == 1,], mean)
str(data_1)

data %>% 
  group_by(win, vote > 55) %>% 
  summarize(count = n())

data_3 <- aggregate(dpass~vote+win, data, mean)
colnames(data_3) <- c("vote", "win", "dpass")
data_3$win <- factor(data_3$win, labels=c("loser", "winer"))


data_3_centered <- data_3 %>% 
  mutate(vote_centered = vote - 55)

model_1_centered_all <- lm(dpass~ vote_centered + win, data = data_3_centered)
tidy(model_1_centered_all)

data_3_centered_20 <- data_3_centered %>% 
  filter(vote_centered > -20 & vote_centered < 20)

data_3_centered_10 <- data_3_centered %>% 
  filter(vote_centered > -10 & vote_centered < 10)

model_2_centered_20 <- lm(dpass ~ vote_centered + win, data = data_3_centered_20)
tidy(model_2_centered_20)

model_3_centered_10 <- lm(dpass ~ vote_centered + win, data = data_3_centered_10)
tidy(model_3_centered_10)

# Unit all 3 model to 1 table for looking result in one table
table_55 <- huxreg(list("Full data" = model_1_centered_all,
                        "data_centered_20" = model_2_centered_20,
                        "data_centered_10" = model_3_centered_10))



plot_55 <- ggplot(data_3[data_3$vote >= 15 & data_3$vote <= 85,], mapping=aes(x = vote, y = dpass, color = win)) +
  geom_point(size = 2, alpha = 0.5) +
  geom_vline(xintercept = 55, linetype = "dashed") +
  geom_smooth() + 
  geom_smooth(method = "lm", se = F)

rdd_object <-rdd_data(y=data_3$dpass,x=data_3$vote,cutpoint=55)
new_table_55 <- rdd_reg_np(rdd_object)

# We repeat for cutoff of 52



data <- read.dta("clark.dta")
data$win <- factor(data$win, levels = c(0, 1), labels = c(0, 1))
data$win[data$vote >52] <- (data$win[data$vote >52] == 1)
data[is.na(data)] = 1


data_1 <- aggregate(vote~dpass, data[data$win == 1,], mean)
str(data_1)

data %>% 
  group_by(win, vote > 52) %>% 
  summarize(count = n())

data_3 <- aggregate(dpass~vote+win, data, mean)
colnames(data_3) <- c("vote", "win", "dpass")
data_3$win <- factor(data_3$win, labels=c("loser", "winer"))


data_3_centered <- data_3 %>% 
  mutate(vote_centered = vote - 52)

model_1_centered_all <- lm(dpass~ vote_centered + win, data = data_3_centered)
tidy(model_1_centered_all)

data_3_centered_20 <- data_3_centered %>% 
  filter(vote_centered > -20 & vote_centered < 20)

data_3_centered_10 <- data_3_centered %>% 
  filter(vote_centered > -10 & vote_centered < 10)

model_2_centered_20 <- lm(dpass ~ vote_centered + win, data = data_3_centered_20)
tidy(model_2_centered_20)

model_3_centered_10 <- lm(dpass ~ vote_centered + win, data = data_3_centered_10)
tidy(model_3_centered_10)

# Unit all 3 model to 1 table for looking result in one table
table_52 <- huxreg(list("Full data" = model_1_centered_all,
                        "data_centered_20" = model_2_centered_20,
                        "data_centered_10" = model_3_centered_10))



plot_52 <- ggplot(data_3[data_3$vote >= 15 & data_3$vote <= 85,], mapping=aes(x = vote, y = dpass, color = win)) +
  geom_point(size = 2, alpha = 0.5) +
  geom_vline(xintercept = 48, linetype = "dashed") +
  geom_smooth() + 
  geom_smooth(method = "lm", se = F)

rdd_object <-rdd_data(y=data_3$dpass,x=data_3$vote,cutpoint=52)
new_table_52 <- rdd_reg_np(rdd_object)

# We repeat for cutoff of 48

data <- read.dta("data/clark.dta")
data$win <- factor(data$win, levels = c(0, 1), labels = c(0, 1))
data$win[data$vote >48] <- (data$win[data$vote >48] == 1)
data[is.na(data)] = 1


data_1 <- aggregate(vote~dpass, data[data$win == 1,], mean)
str(data_1)

data %>% 
  group_by(win, vote > 48) %>% 
  summarize(count = n())

data_3 <- aggregate(dpass~vote+win, data, mean)
colnames(data_3) <- c("vote", "win", "dpass")
data_3$win <- factor(data_3$win, labels=c("loser", "winer"))


data_3_centered <- data_3 %>% 
  mutate(vote_centered = vote - 48)

model_1_centered_all <- lm(dpass~ vote_centered + win, data = data_3_centered)
tidy(model_1_centered_all)

data_3_centered_20 <- data_3_centered %>% 
  filter(vote_centered > -20 & vote_centered < 20)

data_3_centered_10 <- data_3_centered %>% 
  filter(vote_centered > -10 & vote_centered < 10)

model_2_centered_20 <- lm(dpass ~ vote_centered + win, data = data_3_centered_20)
tidy(model_2_centered_20)

model_3_centered_10 <- lm(dpass ~ vote_centered + win, data = data_3_centered_10)
tidy(model_3_centered_10)

# Unit all 3 model to 1 table for looking result in one table
table_48 <- huxreg(list("Full data" = model_1_centered_all,
                        "data_centered_20" = model_2_centered_20,
                        "data_centered_10" = model_3_centered_10))



plot_48 <- ggplot(data_3[data_3$vote >= 15 & data_3$vote <= 85,], mapping=aes(x = vote, y = dpass, color = win)) +
  geom_point(size = 2, alpha = 0.5) +
  geom_vline(xintercept = 48, linetype = "dashed") +
  geom_smooth() + 
  geom_smooth(method = "lm", se = F)

rdd_object <-rdd_data(y=data_3$dpass,x=data_3$vote,cutpoint=48)
new_table_48 <- rdd_reg_np(rdd_object)


#So the only statistically significant result at the 5% level is that for a cut off of 50
#This is what we want to see. It shows that the cut off of 50% is not arbitrary, and that the result of the vote DOES make a difference
#If we had significant results for some other values (particular if they were far from 50) we might conlcude that it is not the vote that is important but rather some other factor
# Check that as we approach 50 our estimate, p-value etc converge to those given by cut off of 50

rdd_object <-rdd_data(y=data_3$dpass,x=data_3$vote,cutpoint=50.1)
new_table_50.1 <- rdd_reg_np(rdd_object)

#our estimate for 50.1 is 3.7589 (as opposed to 3.3692 for 50) and the P-value is 0.002027 (as opposed to 0.005703 for 50)
```

